# EMRX Token

## Inofmration
```
Name : Emirex Token
Symbol : EMRX
Type : Burnable, pausable, owned ERC-20 token
Emission : 500 000 000
Decimals : 8
Blockchain : Ethereum Mainnet
Vesting : Vesting on Vesting contract
```

## Deployment

Ethereum mainnet
```
EMRX (Proxy): 0xbdbC2a5B32F3a5141ACd18C39883066E4daB9774

ERC20 EMRX (Implementation): 0x5b907b82a7c69f7b80c1f467c089a876c8a21dfa

Vesting (Proxy) : 
0x67D26E94055220fbae2805A3B86E5EFB78054c53

Vesting (Implementation)
0x3747f235d1e7ae96ebd54bc049cc68b2714d7586
```

## Code

PROXY : https://etherscan.io/address/0xbdbC2a5B32F3a5141ACd18C39883066E4daB9774#code

ERC20 (ABI): https://etherscan.io/address/0x5B907B82a7C69f7b80c1F467c089a876c8a21dFa#code

Vesting Proxy : 
https://etherscan.io/address/0x67D26E94055220fbae2805A3B86E5EFB78054c53#code

Vesting implementation : https://etherscan.io/address/0x3747f235d1e7ae96ebd54bc049cc68b2714d7586#code


## Token Explorer

EMRX : https://etherscan.io/token/0xbdbC2a5B32F3a5141ACd18C39883066E4daB9774

## Documenation 

[EMRX ABI Documentation](./docs/EMRX.md)

