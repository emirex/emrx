# Interacting with EMRX Vesting contract

## Etherscan

Please open 
https://etherscan.io/address/0x67D26E94055220fbae2805A3B86E5EFB78054c53#code 

Click Contract section

Check "implementaion". This value points to Vestign contract implementation

![image](./img/img2.png)

Open Vesting implementation : https://etherscan.io/address/0x3747f235d1e7ae96ebd54bc049cc68b2714d7586#code

You can see the source code and Vestign contract ABI. 
Copy ABI to clipboard.

![image](./img/img3.png)

## MyEtherWallet

Open Contract section in MEW.

![image](./img/img1.png)

Copy ABI from clipboard and set address to Vestign proxy address : 0x67D26E94055220fbae2805A3B86E5EFB78054c53

Click Continue.


![image](./img/img4.png)

Now you can read variables from Vestign contract. Please choose function from list and copy and paste your Ethereum address used for Vesting of EMRX tokens. 

![image](./img/img5.png)

Following functions will provide you with complete set of information:

getAmount(address) -> amount of EMRX granted to you (To calculate number of EMRX tokens please divide this number to 100000000). 

getAvailable(address) -> amount of EMRX available for withdrawal (To calculate number of EMRX tokens please divide this number to 100000000). 


getPeriod(address) -> Number of days in your Vesting period

getReleasePeriods(address) -> Number of releases during the period. 

```
1000 EMRX tokens (100000000000 in terms of getAmount and getAvailable) granted for 100 days with 50 release periods means 20 EMRX will be available for withdrawal each 2 days.  
````

getCurrentPeriod(address) - > current period
```
For example above current period equal to 10 means you can withdraw 200 EMRX. 
```


getStartTime(address) -> start of vesting period in Seconds from 1 Jan 1970 (UNIX timestamp)

withdraw() -> withdraw all avaialble EMRX tokens to your Address. Amount of EMRX will appear on your account. 
