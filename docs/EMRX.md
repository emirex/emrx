* [EMRX](#emrx)
  * [name](#function-name)
  * [approve](#function-approve)
  * [bulkTransfer](#function-bulktransfer)
  * [totalSupply](#function-totalsupply)
  * [transferFrom](#function-transferfrom)
  * [burnLimit](#function-burnlimit)
  * [decimals](#function-decimals)
  * [increaseAllowance](#function-increaseallowance)
  * [unpause](#function-unpause)
  * [burn](#function-burn)
  * [isPauser](#function-ispauser)
  * [paused](#function-paused)
  * [initOwnership](#function-initownership)
  * [removePauser](#function-removepauser)
  * [renouncePauser](#function-renouncepauser)
  * [balanceOf](#function-balanceof)
  * [renounceOwnership](#function-renounceownership)
  * [burnFrom](#function-burnfrom)
  * [setTrustedAddress](#function-settrustedaddress)
  * [initialize](#function-initialize)
  * [addPauser](#function-addpauser)
  * [pause](#function-pause)
  * [reclaimToken](#function-reclaimtoken)
  * [owner](#function-owner)
  * [isOwner](#function-isowner)
  * [symbol](#function-symbol)
  * [reclaimEther](#function-reclaimether)
  * [decreaseAllowance](#function-decreaseallowance)
  * [getTest](#function-gettest)
  * [transfer](#function-transfer)
  * [allowance](#function-allowance)
  * [transferOwnership](#function-transferownership)
  * [OwnershipTransferred](#event-ownershiptransferred)
  * [Paused](#event-paused)
  * [Unpaused](#event-unpaused)
  * [PauserAdded](#event-pauseradded)
  * [PauserRemoved](#event-pauserremoved)
  * [Transfer](#event-transfer)
  * [Approval](#event-approval)

# EMRX


## *function* name

EMRX.name() `view` `06fdde03`





## *function* approve

EMRX.approve(spender, value) `nonpayable` `095ea7b3`

> See `IERC20.approve`.     * Requirements:     * - `spender` cannot be the zero address.

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | spender | undefined |
| *uint256* | value | undefined |


## *function* bulkTransfer

EMRX.bulkTransfer(recipients, amounts) `nonpayable` `153a1f3e`

> Bulk transfer function     * Makes multiple transfers to receipients. tokenFallback function isn't called for trusted smart contracts.

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address[]* | recipients | undefined |
| *uint256[]* | amounts | undefined |


## *function* totalSupply

EMRX.totalSupply() `view` `18160ddd`

> See `IERC20.totalSupply`.




## *function* transferFrom

EMRX.transferFrom(sender, recipient, amount) `nonpayable` `23b872dd`

> See `IERC20.transferFrom`.     * Emits an `Approval` event indicating the updated allowance. This is not required by the EIP. See the note at the beginning of `ERC20`;     * Requirements: - `sender` and `recipient` cannot be the zero address. - `sender` must have a balance of at least `value`. - the caller must have allowance for `sender`'s tokens of at least `amount`.

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | sender | undefined |
| *address* | recipient | undefined |
| *uint256* | amount | undefined |


## *function* burnLimit

EMRX.burnLimit() `view` `2a9c72c8`





## *function* decimals

EMRX.decimals() `view` `313ce567`

> Returns the number of decimals used to get its user representation. For example, if `decimals` equals `2`, a balance of `505` tokens should be displayed to a user as `5,05` (`505 / 10 ** 2`).     * Tokens usually opt for a value of 18, imitating the relationship between Ether and Wei.     * > Note that this information is only used for _display_ purposes: it in no way affects any of the arithmetic of the contract, including `IERC20.balanceOf` and `IERC20.transfer`.




## *function* increaseAllowance

EMRX.increaseAllowance(spender, addedValue) `nonpayable` `39509351`

> Atomically increases the allowance granted to `spender` by the caller.     * This is an alternative to `approve` that can be used as a mitigation for problems described in `IERC20.approve`.     * Emits an `Approval` event indicating the updated allowance.     * Requirements:     * - `spender` cannot be the zero address.

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | spender | undefined |
| *uint256* | addedValue | undefined |


## *function* unpause

EMRX.unpause() `nonpayable` `3f4ba83a`

> Called by a pauser to unpause, returns to normal state.




## *function* burn

EMRX.burn(amount) `nonpayable` `42966c68`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *uint256* | amount | undefined |


## *function* isPauser

EMRX.isPauser(account) `view` `46fbf68e`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | account | undefined |


## *function* paused

EMRX.paused() `view` `5c975abb`

> Returns true if the contract is paused, and false otherwise.




## *function* initOwnership

EMRX.initOwnership(newOwner) `nonpayable` `61302f17`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | newOwner | undefined |


## *function* removePauser

EMRX.removePauser(account) `nonpayable` `6b2c0f55`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | account | undefined |


## *function* renouncePauser

EMRX.renouncePauser() `nonpayable` `6ef8d66d`





## *function* balanceOf

EMRX.balanceOf(account) `view` `70a08231`

> See `IERC20.balanceOf`.

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | account | undefined |


## *function* renounceOwnership

EMRX.renounceOwnership() `nonpayable` `715018a6`

> Leaves the contract without owner. It will not be possible to call `onlyOwner` functions anymore. Can only be called by the current owner.     * > Note: Renouncing ownership will leave the contract without an owner, thereby removing any functionality that is only available to the owner.




## *function* burnFrom

EMRX.burnFrom(account, amount) `nonpayable` `79cc6790`

> See `ERC20._burnFrom`.

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | account | undefined |
| *uint256* | amount | undefined |


## *function* setTrustedAddress

EMRX.setTrustedAddress(trusted, is_trusted) `nonpayable` `806c3ca9`

> set trusted certificate address, containing tokenFallback function implementatios     * Requirements:     * - `recipient` cannot be the zero address. - the caller must have a balance of at least `amount`.

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | trusted | undefined |
| *bool* | is_trusted | undefined |


## *function* initialize

EMRX.initialize() `nonpayable` `8129fc1c`





## *function* addPauser

EMRX.addPauser(account) `nonpayable` `82dc1ec4`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | account | undefined |


## *function* pause

EMRX.pause() `nonpayable` `8456cb59`

> Called by a pauser to pause, triggers stopped state.




## *function* reclaimToken

EMRX.reclaimToken(token, _to) `nonpayable` `88ee39cc`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | token | undefined |
| *address* | _to | undefined |


## *function* owner

EMRX.owner() `view` `8da5cb5b`

> Returns the address of the current owner.




## *function* isOwner

EMRX.isOwner() `view` `8f32d59b`

> Returns true if the caller is the current owner.




## *function* symbol

EMRX.symbol() `view` `95d89b41`

> Returns the symbol of the token, usually a shorter version of the name.




## *function* reclaimEther

EMRX.reclaimEther(_to) `nonpayable` `9a6a30a4`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _to | undefined |


## *function* decreaseAllowance

EMRX.decreaseAllowance(spender, subtractedValue) `nonpayable` `a457c2d7`

> Atomically decreases the allowance granted to `spender` by the caller.     * This is an alternative to `approve` that can be used as a mitigation for problems described in `IERC20.approve`.     * Emits an `Approval` event indicating the updated allowance.     * Requirements:     * - `spender` cannot be the zero address. - `spender` must have allowance for the caller of at least `subtractedValue`.

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | spender | undefined |
| *uint256* | subtractedValue | undefined |


## *function* getTest

EMRX.getTest() `pure` `a8cd0a80`





## *function* transfer

EMRX.transfer(recipient, amount) `nonpayable` `a9059cbb`

> See `IERC20.transfer`.     * Requirements:     * - `recipient` cannot be the zero address. - the caller must have a balance of at least `amount`.

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | recipient | undefined |
| *uint256* | amount | undefined |


## *function* allowance

EMRX.allowance(owner, spender) `view` `dd62ed3e`

> See `IERC20.allowance`.

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | owner | undefined |
| *address* | spender | undefined |


## *function* transferOwnership

EMRX.transferOwnership(newOwner) `nonpayable` `f2fde38b`

> Transfers ownership of the contract to a new account (`newOwner`). Can only be called by the current owner.

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | newOwner | undefined |


## *event* OwnershipTransferred

EMRX.OwnershipTransferred(previousOwner, newOwner) `8be0079c`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | previousOwner | indexed |
| *address* | newOwner | indexed |

## *event* Paused

EMRX.Paused(account) `62e78cea`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | account | not indexed |

## *event* Unpaused

EMRX.Unpaused(account) `5db9ee0a`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | account | not indexed |

## *event* PauserAdded

EMRX.PauserAdded(account) `6719d08c`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | account | indexed |

## *event* PauserRemoved

EMRX.PauserRemoved(account) `cd265eba`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | account | indexed |

## *event* Transfer

EMRX.Transfer(from, to, value) `ddf252ad`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | from | indexed |
| *address* | to | indexed |
| *uint256* | value | not indexed |

## *event* Approval

EMRX.Approval(owner, spender, value) `8c5be1e5`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | owner | indexed |
| *address* | spender | indexed |
| *uint256* | value | not indexed |


---